using System.Collections.Generic;
using System.Threading.Tasks;
using DKX.JsonApi;
using DKX.JsonApiTests.App;
using Xunit;

namespace DKX.JsonApiTests
{
	public class ServerTest : BaseTest
	{
		private readonly IList<string> _actions = new List<string>();

		private readonly Server _server;

		public ServerTest()
		{
			_server = ServerFactory.Create(_actions);
		}
			
		[Fact]
		public async Task TransformItem()
		{
			var book = new Book("42", "Harry Potter");
			var result = await _server.TransformItem("v1", book);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""books"",
    ""id"": ""42"",
    ""attributes"": {
      ""content"": null,
      ""title"": ""Harry Potter""
    }
  }
}",
				result
			);
		}
			
		[Fact]
		public async Task TransformCollection()
		{
			var result = await _server.TransformCollection("v1", new []
			{
				new Book("42", "Harry Potter"),
				new Book("84", "Lord of the Rings"),
			});
			
			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""books"",
      ""id"": ""42"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Harry Potter""
      }
    },
    {
      ""type"": ""books"",
      ""id"": ""84"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Lord of the Rings""
      }
    }
  ]
}",
				result
			);
		}

		[Fact]
		public async Task TransformCollection_Relationship_Item()
		{
			var result = await _server.TransformCollection("v1", new []
			{
				new Book("42", "Harry Potter"), 
				new Book("84", "Lord of the Rings")
				{
					Author = new User("5", "J. K. Rowling"),
				}, 
			}, new Context().WithIncludes("author"));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""books"",
      ""id"": ""42"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Harry Potter""
      },
      ""relationships"": {
        ""author"": {
          ""data"": null
        }
      }
    },
    {
      ""type"": ""books"",
      ""id"": ""84"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Lord of the Rings""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""5""
          }
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""users"",
      ""id"": ""5"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""J. K. Rowling""
      }
    }
  ]
}",
				result
			);
		}

		[Fact]
		public async Task TransformCollection_Relationship_Collection()
		{
			var result = await _server.TransformCollection("v1", new []
			{
				new Book("42", "Harry Potter"),
				new Book("84", "Lord of the Rings")
				{
					Chapters = new []
					{
						new Chapter("1", "1st chapter"),
						new Chapter("2", "2nd chapter"),
					},
				},
			}, new Context().WithIncludes("chapters"));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""books"",
      ""id"": ""42"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Harry Potter""
      },
      ""relationships"": {
        ""chapters"": {
          ""data"": []
        }
      }
    },
    {
      ""type"": ""books"",
      ""id"": ""84"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Lord of the Rings""
      },
      ""relationships"": {
        ""chapters"": {
          ""data"": [
            {
              ""type"": ""chapters"",
              ""id"": ""1""
            },
            {
              ""type"": ""chapters"",
              ""id"": ""2""
            }
          ]
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""chapters"",
      ""id"": ""1"",
      ""attributes"": {
        ""title"": ""1st chapter""
      }
    },
    {
      ""type"": ""chapters"",
      ""id"": ""2"",
      ""attributes"": {
        ""title"": ""2nd chapter""
      }
    }
  ]
}",
				result
			);
		}

		[Fact]
		public async Task TransformItem_Relations_Deferred()
		{
			var book = new Book("42", "Lord of the Rings")
			{
				Editor = new User("3", "John Doe"),
				Author = new User("1", "J. R. R. Tolkien"),
			};
			
			var result = await _server.TransformItem("v1", book, new Context().WithIncludes("author", "editor"));
			
			Assert.Equal(new List<string>
			{
				"users:load(J. R. R. Tolkien)",
				"users:extract(J. R. R. Tolkien)",
				"users:load(John Doe)",
				"users:extract(John Doe)",
			}, _actions);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""books"",
    ""id"": ""42"",
    ""attributes"": {
      ""content"": null,
      ""title"": ""Lord of the Rings""
    },
    ""relationships"": {
      ""author"": {
        ""data"": {
          ""type"": ""users"",
          ""id"": ""1""
        }
      },
      ""editor"": {
        ""data"": {
          ""type"": ""users"",
          ""id"": ""3""
        }
      }
    }
  },
  ""included"": [
    {
      ""type"": ""users"",
      ""id"": ""1"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""J. R. R. Tolkien""
      }
    },
    {
      ""type"": ""users"",
      ""id"": ""3"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""John Doe""
      }
    }
  ]
}",
				result
			);
		}
		
		[Fact]
		public async Task TransformCollection_Relations_Complex1()
		{
			var roleAuthor = new Role("222", "author");
			var roleEditor = new Role("333", "editor");
			
			var tolkien = new User("1", "J. R. R. Tolkien")
			{
				Role = roleAuthor,
			};
			
			var rowling = new User("2", "J. K. Rowling")
			{
				Role = roleAuthor,
			};
			
			var editor = new User("3", "John Doe")
			{
				Role = roleEditor,
			};
			
			var result = await _server.TransformCollection("v1", new[]
			{
				new Book("42", "Harry Potter")
				{
					Author = rowling,
					Editor = editor,
					Chapters = new []
					{
						new Chapter("1a", "Chapter 1")
						{
							Author = rowling,
							Editor = editor,
						}, 
					},
				},
				new Book("84", "Lord of the Rings")
				{
					Author = tolkien,
					Editor = editor,
					Chapters = new[]
					{
						new Chapter("1", "1st chapter")
						{
							Author = tolkien,
							Editor = editor,
						},
						new Chapter("2", "2nd chapter")
						{
							Author = tolkien,
							Editor = editor,
						},
					},
				},
			}, new Context().WithIncludes("author", "editor.role", "chapters.author", "chapters.editor.role"));

			Assert.Equal(new List<string>
			{
				// 1st cycle
				"users:load(J. K. Rowling, J. R. R. Tolkien)",
				"users:extract(J. K. Rowling)",
				"users:extract(J. R. R. Tolkien)",
				"users:load(John Doe, John Doe)",
				"users:extract(John Doe)",
				"users:extract(John Doe)",
				"chapters:load(1st chapter, 2nd chapter, Chapter 1)",
				"chapters:extract(Chapter 1)",
				"chapters:extract(1st chapter, 2nd chapter)",
				// 2nd cycle
				"roles:load(editor, editor)",
				"roles:extract(editor)",
				"roles:extract(editor)",
				"users:load(J. K. Rowling, J. R. R. Tolkien, J. R. R. Tolkien)",
				"users:extract(J. K. Rowling)",
				"users:extract(J. R. R. Tolkien)",
				"users:extract(J. R. R. Tolkien)",
				"users:load(John Doe, John Doe, John Doe)",
				"users:extract(John Doe)",
				"users:extract(John Doe)",
				"users:extract(John Doe)",
				// 3rd cycle
				"roles:load(editor, editor, editor)",
				"roles:extract(editor)",
				"roles:extract(editor)",
				"roles:extract(editor)",
			}, _actions);

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""books"",
      ""id"": ""42"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Harry Potter""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""2""
          }
        },
        ""chapters"": {
          ""data"": [
            {
              ""type"": ""chapters"",
              ""id"": ""1a""
            }
          ]
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""books"",
      ""id"": ""84"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Lord of the Rings""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""1""
          }
        },
        ""chapters"": {
          ""data"": [
            {
              ""type"": ""chapters"",
              ""id"": ""1""
            },
            {
              ""type"": ""chapters"",
              ""id"": ""2""
            }
          ]
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""chapters"",
      ""id"": ""1"",
      ""attributes"": {
        ""title"": ""1st chapter""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""1""
          }
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""chapters"",
      ""id"": ""1a"",
      ""attributes"": {
        ""title"": ""Chapter 1""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""2""
          }
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""chapters"",
      ""id"": ""2"",
      ""attributes"": {
        ""title"": ""2nd chapter""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""1""
          }
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""roles"",
      ""id"": ""333"",
      ""attributes"": {
        ""name"": ""editor""
      }
    },
    {
      ""type"": ""users"",
      ""id"": ""1"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""J. R. R. Tolkien""
      }
    },
    {
      ""type"": ""users"",
      ""id"": ""2"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""J. K. Rowling""
      }
    },
    {
      ""type"": ""users"",
      ""id"": ""3"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""John Doe""
      },
      ""relationships"": {
        ""role"": {
          ""data"": {
            ""type"": ""roles"",
            ""id"": ""333""
          }
        }
      }
    }
  ]
}",
				result
			);
		}
		
		[Fact]
		public async Task TransformCollection_Relations_Complex2()
		{
			var roleAuthor = new Role("222", "author");
			var roleEditor = new Role("333", "editor");
			
			var editor = new User("3", "John Doe")
			{
				Role = roleEditor,
			};
			
			var tolkien = new User("1", "J. R. R. Tolkien")
			{
				Role = roleAuthor,
			};
			
			var rowling = new User("2", "J. K. Rowling")
			{
				Role = roleAuthor,
			};
			
			var result = await _server.TransformCollection("v1", new[]
			{
				new Book("42", "Harry Potter")
				{
					Author = rowling,
					Editor = editor,
					Chapters = new []
					{
						new Chapter("1a", "Chapter 1")
						{
							Author = rowling,
							Editor = editor,
						}, 
					},
				},
				new Book("84", "Lord of the Rings")
				{
					Author = tolkien,
					Editor = editor,
					Chapters = new[]
					{
						new Chapter("1", "1st chapter")
						{
							Author = tolkien,
							Editor = editor,
						},
						new Chapter("2", "2nd chapter")
						{
							Author = tolkien,
							Editor = editor,
						},
					},
				},
			}, new Context().WithIncludes("author", "editor.role", "chapters.author", "chapters.editor.role"));

			Assert.Equal(new List<string>
			{
				// 1st cycle
				"users:load(J. K. Rowling, J. R. R. Tolkien)",
				"users:extract(J. K. Rowling)",
				"users:extract(J. R. R. Tolkien)",
				"users:load(John Doe, John Doe)",
				"users:extract(John Doe)",
				"users:extract(John Doe)",
				"chapters:load(1st chapter, 2nd chapter, Chapter 1)",
				"chapters:extract(Chapter 1)",
				"chapters:extract(1st chapter, 2nd chapter)",
				// 2nd cycle
				"roles:load(editor, editor)",
				"roles:extract(editor)",
				"roles:extract(editor)",
				"users:load(J. K. Rowling, J. R. R. Tolkien, J. R. R. Tolkien)",
				"users:extract(J. K. Rowling)",
				"users:extract(J. R. R. Tolkien)",
				"users:extract(J. R. R. Tolkien)",
				"users:load(John Doe, John Doe, John Doe)",
				"users:extract(John Doe)",
				"users:extract(John Doe)",
				"users:extract(John Doe)",
				// 3rd cycle
				"roles:load(editor, editor, editor)",
				"roles:extract(editor)",
				"roles:extract(editor)",
				"roles:extract(editor)",
			}, _actions);

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""books"",
      ""id"": ""42"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Harry Potter""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""2""
          }
        },
        ""chapters"": {
          ""data"": [
            {
              ""type"": ""chapters"",
              ""id"": ""1a""
            }
          ]
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""books"",
      ""id"": ""84"",
      ""attributes"": {
        ""content"": null,
        ""title"": ""Lord of the Rings""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""1""
          }
        },
        ""chapters"": {
          ""data"": [
            {
              ""type"": ""chapters"",
              ""id"": ""1""
            },
            {
              ""type"": ""chapters"",
              ""id"": ""2""
            }
          ]
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""chapters"",
      ""id"": ""1"",
      ""attributes"": {
        ""title"": ""1st chapter""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""1""
          }
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""chapters"",
      ""id"": ""1a"",
      ""attributes"": {
        ""title"": ""Chapter 1""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""2""
          }
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""chapters"",
      ""id"": ""2"",
      ""attributes"": {
        ""title"": ""2nd chapter""
      },
      ""relationships"": {
        ""author"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""1""
          }
        },
        ""editor"": {
          ""data"": {
            ""type"": ""users"",
            ""id"": ""3""
          }
        }
      }
    },
    {
      ""type"": ""roles"",
      ""id"": ""333"",
      ""attributes"": {
        ""name"": ""editor""
      }
    },
    {
      ""type"": ""users"",
      ""id"": ""1"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""J. R. R. Tolkien""
      }
    },
    {
      ""type"": ""users"",
      ""id"": ""2"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""J. K. Rowling""
      }
    },
    {
      ""type"": ""users"",
      ""id"": ""3"",
      ""attributes"": {
        ""email"": null,
        ""name"": ""John Doe""
      },
      ""relationships"": {
        ""role"": {
          ""data"": {
            ""type"": ""roles"",
            ""id"": ""333""
          }
        }
      }
    }
  ]
}",
				result
			);
		}
		
		[Fact]
		public async Task TransformItem_SpecifyFields()
		{
			var result = await _server.TransformItem(
				"v1",
				new Book("42", "Harry Potter")
				{
					Content = "lorem ipsum",
					Author = new User("5", "J. K. Rowling")
					{
						Email = "j.k.@rowling.com",
					},
				},
				new Context()
					.WithIncludes("author")
					.WithFields("users", "email")
			);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""books"",
    ""id"": ""42"",
    ""attributes"": {
      ""content"": ""lorem ipsum"",
      ""title"": ""Harry Potter""
    },
    ""relationships"": {
      ""author"": {
        ""data"": {
          ""type"": ""users"",
          ""id"": ""5""
        }
      }
    }
  },
  ""included"": [
    {
      ""type"": ""users"",
      ""id"": ""5"",
      ""attributes"": {
        ""email"": ""j.k.@rowling.com""
      }
    }
  ]
}",
				result
			);
		}
	}
}
