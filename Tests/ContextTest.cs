using System.Collections.Generic;
using System.Collections.Immutable;
using DKX.JsonApi;
using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiTests
{
	public class ContextTest
	{
		[Fact]
		public void Test()
		{
			IContext context = new Context();
			
			var contextWithVersion = context.WithVersion("v2");

			Assert.Equal("v2", contextWithVersion.Version);

			var contextWithIncludes = context.WithIncludes("user", "user.role");

			Assert.Equal(new[] {"user", "user.role"}, contextWithIncludes.Includes);

			var contextWithFields = context
				.WithFields("user", "email", "name")
				.WithFields("role", "name");

			Assert.Equal(new Dictionary<string, ImmutableArray<string>>
			{
				["user"] = ImmutableArray.Create("email", "name"),
				["role"] = ImmutableArray.Create("name"),
			}, contextWithFields.Fields);

			var meta = new Struct();
			var contextWithMeta = context.WithMeta(meta);

			Assert.Same(meta, contextWithMeta.Meta);

			Assert.Equal("v1", context.Version);
			Assert.Null(context.Includes);
			Assert.Null(context.Fields);
			Assert.Null(context.Meta);
		}
	}
}
