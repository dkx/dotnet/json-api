using System.Globalization;

namespace DKX.JsonApiTests
{
	public abstract class BaseTest
	{
		protected BaseTest()
		{
			CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
		}
	}
}
