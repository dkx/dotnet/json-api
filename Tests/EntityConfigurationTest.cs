using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.DI;
using DKX.JsonApi.Relationships;
using DKX.JsonApi.Relationships.Loaders;
using DKX.JsonApiSerializer;
using Xunit;

namespace DKX.JsonApiTests
{
	public class EntityConfigurationTest
	{
		private readonly Server _server;
		
		private readonly StaticServiceFactory _serviceFactory = new StaticServiceFactory();
		
		private readonly IEntityMapperFactory _entityMapperFactory = new DefaultEntityMapperFactory();

		private readonly IValueLoader _valueLoader;

		public EntityConfigurationTest()
		{
			_serviceFactory.Add(new UserLoader());
			_serviceFactory.Add(new GroupLoader());
			_server = new Server(_serviceFactory, new Serializer());
			_valueLoader = new ValueLoader(_serviceFactory);
		}

		[Fact]
		public void Throws_MissingResource()
		{
			var e = Assert.Throws<ArgumentException>(() => new EntityConfiguration<EntityConfigurationTest>(_server, _serviceFactory, _entityMapperFactory, _valueLoader, GetType()));
			Assert.Equal("Class DKX.JsonApiTests.EntityConfigurationTest is not valid entity mapper because it is missing a [Resource] attribute", e.Message);
		}

		[Fact]
		public void Throws_MissingId()
		{
			var e = Assert.Throws<Exception>(() => new EntityConfiguration<EntityConfigurationTest>(_server, _serviceFactory, _entityMapperFactory, _valueLoader, typeof(ResourceWithoutId)));
			Assert.Equal("Entity mapper DKX.JsonApiTests.EntityConfigurationTest+ResourceWithoutId does not have any member with [Id] attribute", e.Message);
		}

		[Fact]
		public void Getters_Fields()
		{
			var mapper = new ProjectMappingWithFields(new Context(), new Project());
			var type = mapper.GetType();
			var configuration = new EntityConfiguration<ProjectMappingWithFields>(_server, _serviceFactory, _entityMapperFactory, _valueLoader, type);

			Assert.Equal("v3", configuration.Version);
			Assert.Equal("project", configuration.ResourceName);
			Assert.Same(type.GetField("Id")!, configuration.Id);
			Assert.Equal(2, configuration.Fields.Length);
			Assert.Equal("name", configuration.Fields[0].Name);
			Assert.Equal("link", configuration.Fields[1].Name);
			Assert.Equal("JsonApi", configuration.Fields[0].GetValue(mapper));
			Assert.Equal("gitlab", configuration.Fields[1].GetValue(mapper));
			Assert.Equal(2, configuration.Relationships.Length);
			Assert.Equal("owner", configuration.Relationships[0].Name);
			Assert.Equal("children", configuration.Relationships[1].Name);
			Assert.IsType<EntityRelationship<ProjectMappingWithFields, Guid>>(configuration.Relationships[0]);
			Assert.IsType<EntityRelationship<ProjectMappingWithFields, Guid>>(configuration.Relationships[1]);
		}

		[Fact]
		public void Getters_Properties()
		{
			var mapper = new ProjectMappingWithProperties(new Project());
			var type = mapper.GetType();
			var configuration = new EntityConfiguration<ProjectMappingWithProperties>(_server, _serviceFactory, _entityMapperFactory, _valueLoader, type);

			Assert.Equal("v3", configuration.Version);
			Assert.Equal("project", configuration.ResourceName);
			Assert.Same(type.GetProperty("Id")!, configuration.Id);
			Assert.Equal(2, configuration.Fields.Length);
			Assert.Equal("name", configuration.Fields[0].Name);
			Assert.Equal("link", configuration.Fields[1].Name);
			Assert.Equal("JsonApi", configuration.Fields[0].GetValue(mapper));
			Assert.Equal("gitlab", configuration.Fields[1].GetValue(mapper));
			Assert.Equal(2, configuration.Relationships.Length);
			Assert.Equal("owner", configuration.Relationships[0].Name);
			Assert.Equal("root", configuration.Relationships[1].Name);
			Assert.IsType<EntityRelationship<ProjectMappingWithProperties, Guid>>(configuration.Relationships[0]);
			Assert.IsType<EntityRelationship<ProjectMappingWithProperties, Guid>>(configuration.Relationships[1]);
		}

		[Fact]
		public void CreateEntityAccessor_ContextAware()
		{
			var context = new Context();
			var project = new Project();
			var type = typeof(ProjectMappingWithFields);
			var configuration = new EntityConfiguration<ProjectMappingWithFields>(_server, _serviceFactory, _entityMapperFactory, _valueLoader, type);
			
			configuration.CreateEntityAccessor(context, project);

			Assert.Same(context, project.Context);
		}

		[Resource("v1", "user")]
		private class ResourceWithoutId
		{
		}

		private class Project
		{
			public string Id { get; set; } = "042";
			public string Name { get; set; } = "JsonApi";
			public string Url { get; set; } = "gitlab";

			public IContext? Context { get; set; }
		}

		[Resource("v3", "project")]
		private class ProjectMappingWithFields : IContextAware
		{
			private readonly Project _project;
			
			public ProjectMappingWithFields(IContext context, Project project)
			{
				_project = project;
				Id = project.Id;
				Name = project.Name;
				Url = project.Url;

				project.Context = context;
			}

			[Id]
			public string Id;

			[Field]
			public string Name;

			[Field("link")]
			public string Url;

			[Relationship(typeof(UserLoader))]
			public Guid Owner = default!;

			[Relationship(typeof(GroupLoader), "children")]
			public Guid? Groups = null;
		}

		[Resource("v3", "project")]
		private class ProjectMappingWithProperties
		{
			public ProjectMappingWithProperties(Project project)
			{
				Id = project.Id;
				Name = project.Name;
				Url = project.Url;
			}

			[Id]
			public string Id { get; set; }

			[Field]
			public string Name { get; set; }

			[Field("link")]
			public string Url { get; set; }

			[Relationship(typeof(UserLoader))]
			public Guid Owner { get; set; } = default!;

			[Relationship(typeof(GroupLoader), "root")]
			public Guid? Group { get; set; } = null;
		}

		private class User
		{
		}

		private class UserLoader : IItemRelationshipLoader<Guid, User>
		{
			public Task<IEnumerable<User>> Load(IContext context, IEnumerable<Guid> keys, CancellationToken cancellationToken)
			{
				throw new NotImplementedException();
			}

			public User Extract(IContext context, Guid key, IEnumerable<User> data)
			{
				throw new NotImplementedException();
			}
		}

		private class Group
		{
		}

		private class GroupLoader : ICollectionRelationshipLoader<Guid, Group>
		{
			public Task<IEnumerable<Group>> Load(IContext context, IEnumerable<Guid> keys, CancellationToken cancellationToken)
			{
				throw new NotImplementedException();
			}

			public IEnumerable<Group> Extract(IContext context, Guid key, IEnumerable<Group> data)
			{
				throw new NotImplementedException();
			}
		}
	}
}
