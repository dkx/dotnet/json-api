using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi;
using DKX.JsonApi.Relationships.Loaders;

namespace DKX.JsonApiTests.App.RelationshipLoaders
{
	public class ChaptersLoaderByChapters : ICollectionRelationshipLoader<IEnumerable<Chapter>, Chapter>
	{
		private readonly IList<string> _actions;
		
		public ChaptersLoaderByChapters(IList<string> actions)
		{
			_actions = actions;
		}
		
		public Task<IEnumerable<Chapter>> Load(IContext context, IEnumerable<IEnumerable<Chapter>> keys, CancellationToken cancellationToken)
		{
			_actions.Add($"chapters:load({string.Join(", ", keys.SelectMany(k => k.Select(kk => kk.Title)).OrderBy(k => k))})");
			return Task.FromResult(keys.SelectMany(ch => ch));
		}

		public IEnumerable<Chapter> Extract(IContext context, IEnumerable<Chapter> key, IEnumerable<Chapter> data)
		{
			_actions.Add($"chapters:extract({string.Join(", ", key.Select(k => k.Title))})");
			return data.Where(key.Contains!);
		}
	}
}
