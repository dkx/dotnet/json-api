using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi;
using DKX.JsonApi.Relationships.Loaders;

namespace DKX.JsonApiTests.App.RelationshipLoaders
{
	public class UserLoaderByUser : IItemRelationshipLoader<User?, User?>
	{
		private readonly IList<string> _actions;
		
		public UserLoaderByUser(IList<string> actions)
		{
			_actions = actions;
		}

		public Task<IEnumerable<User?>> Load(IContext context, IEnumerable<User?> keys, CancellationToken cancellationToken)
		{
			_actions.Add($"users:load({string.Join(", ", keys.Select(k => k?.Name.ToString() ?? "null").OrderBy(k => k))})");
			return Task.FromResult(keys);
		}

		public User? Extract(IContext context, User? key, IEnumerable<User?> data)
		{
			_actions.Add($"users:extract({key?.Name ?? "null"})");
			return data.FirstOrDefault(d => d == key);
		}
	}
}
