using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi;
using DKX.JsonApi.Relationships.Loaders;

namespace DKX.JsonApiTests.App.RelationshipLoaders
{
	public class RoleLoaderByRole : IItemRelationshipLoader<Role?, Role?>
	{
		private readonly IList<string> _actions;
		
		public RoleLoaderByRole(IList<string> actions)
		{
			_actions = actions;
		}

		public Task<IEnumerable<Role?>> Load(IContext context, IEnumerable<Role?> keys, CancellationToken cancellationToken)
		{
			_actions.Add($"roles:load({string.Join(", ", keys.Select(k => k?.Name ?? "null").OrderBy(k => k))})");
			return Task.FromResult(keys);
		}

		public Role? Extract(IContext context, Role? key, IEnumerable<Role?> data)
		{
			_actions.Add($"roles:extract({key?.Name ?? "null"})");
			return data.FirstOrDefault(r => r == key);
		}
	}
}
