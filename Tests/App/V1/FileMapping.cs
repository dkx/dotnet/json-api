using System;
using DKX.JsonApi.Attributes;

namespace DKX.JsonApiTests.App.V1
{
	[Resource("v1", "files")]
	public class FileMapping
	{
		private readonly File _file;
		
		public FileMapping(File file)
		{
			_file = file;
		}

		[Id]
		public string Id => _file.Id;

		[Field]
		public string Name => _file.Name;

		[Field]
		[Transformer(typeof(DateTimeTransformer))]
		public DateTime CreatedAt => _file.CreatedAt;
	}
}
