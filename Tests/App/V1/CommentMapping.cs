using DKX.JsonApi.Attributes;
using DKX.JsonApiTests.App.RelationshipLoaders;

namespace DKX.JsonApiTests.App.V1
{
	[Resource("v1", "comments")]
	public class CommentMapping
	{
		private readonly Comment _comment;

		public CommentMapping(Comment comment)
		{
			_comment = comment;
		}

		[Id]
		public string Id => _comment.Id;

		[Field] public string Content => _comment.Content;

		[Relationship(typeof(UserLoaderByUser))]
		public User? Author => _comment.Author;
	}
}
