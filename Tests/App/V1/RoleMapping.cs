using DKX.JsonApi.Attributes;

namespace DKX.JsonApiTests.App.V1
{
	[Resource("v1", "roles")]
	public class RoleMapping
	{
		private readonly Role _role;

		public RoleMapping(Role role)
		{
			_role = role;
		}

		[Id]
		public string Id => _role.Id;

		[Field]
		public string Name => _role.Name;
	}
}
