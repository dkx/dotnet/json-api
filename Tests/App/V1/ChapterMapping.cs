using DKX.JsonApi.Attributes;
using DKX.JsonApiTests.App.RelationshipLoaders;

namespace DKX.JsonApiTests.App.V1
{
	[Resource("v1", "chapters")]
	public class ChapterMapping
	{
		private readonly Chapter _chapter;

		public ChapterMapping(Chapter chapter)
		{
			_chapter = chapter;
		}

		[Id]
		public string Id => _chapter.Id;

		[Field]
		public string Title => _chapter.Title;

		[Relationship(typeof(UserLoaderByUser))]
		public User? Author => _chapter.Author;

		[Relationship(typeof(UserLoaderByUser))]
		public User? Editor => _chapter.Editor;
	}
}
