using DKX.JsonApi.Attributes;
using DKX.JsonApiTests.App.RelationshipLoaders;

namespace DKX.JsonApiTests.App.V1
{
	[Resource("v1", "users")]
	public class UserMapping
	{
		private readonly User _user;

		public UserMapping(User user)
		{
			_user = user;
		}

		[Id]
		public string Id => _user.Id;

		[Field]
		public string Name => _user.Name;

		[Field]
		public string? Email => _user.Email;

		[Relationship(typeof(RoleLoaderByRole))]
		public Role? Role => _user.Role;
	}
}
