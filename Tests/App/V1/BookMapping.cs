using System.Collections.Generic;
using DKX.JsonApi.Attributes;
using DKX.JsonApiTests.App.RelationshipLoaders;

namespace DKX.JsonApiTests.App.V1
{
	[Resource("v1", "books")]
	public class BookMapping
	{
		private readonly Book _book;

		public BookMapping(Book book)
		{
			_book = book;
		}

		[Id]
		public string Id => _book.Id;

		[Field]
		public string Title => _book.Title;

		[Field]
		public string? Content => _book.Content;

		[Relationship(typeof(UserLoaderByUser))]
		public User? Author => _book.Author;

		[Relationship(typeof(UserLoaderByUser))]
		public User? Editor => _book.Editor;

		[Relationship(typeof(ChaptersLoaderByChapters))]
		public IEnumerable<Chapter> Chapters => _book.Chapters ?? new Chapter[] { };
	}
}
