using DKX.JsonApi.Attributes;

namespace DKX.JsonApiTests.App
{
	[Resource("v1", "comments")]
	public class Comment
	{
		public Comment(string id, string content)
		{
			Id = id;
			Content = content;
		}

		[Id]
		public string Id { get; }

		[Field]
		public string Content { get; }
		
		public User? Author { get; set; }
	}
}
