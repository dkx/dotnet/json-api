namespace DKX.JsonApiTests.App
{
	public class User
	{
		public User(string id, string name)
		{
			Id = id;
			Name = name;
		}

		public string Id { get; }
		
		public string Name { get; }
		
		public string? Email { get; set; }
		
		public Role? Role { get; set; }
	}
}
