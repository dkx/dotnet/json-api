using System;
using DKX.JsonApi.Attributes;

namespace DKX.JsonApiTests.App
{
	[Resource("v1", "files")]
	public class File
	{
		public File(string id, string name, DateTime createdAt)
		{
			Id = id;
			Name = name;
			CreatedAt = createdAt;
		}
		
		[Id]
		public string Id { get; }

		[Field]
		public string Name { get; }
		
		[Field]
		[Transformer(typeof(DateTimeTransformer))]
		public DateTime CreatedAt { get; }
	}
}
