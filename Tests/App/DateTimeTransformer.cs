using System;
using DKX.JsonApi;

namespace DKX.JsonApiTests.App
{
	public class DateTimeTransformer : IFieldTransformer<DateTime, string>
	{
		public string Transform(DateTime value)
		{
			return value.ToString("o");
		}
	}
}
