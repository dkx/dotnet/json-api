using System.Collections.Generic;

namespace DKX.JsonApiTests.App
{
	public class Book
	{
		public Book(string id, string title)
		{
			Id = id;
			Title = title;
		}
		
		public string Id { get; }
		
		public string Title { get; }
		
		public string? Content { get; set; }
		
		public User? Author { get; set; }
		
		public User? Editor { get; set; }
		
		public IEnumerable<Chapter>? Chapters { get; set; }
	}
}
