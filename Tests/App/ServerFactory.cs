using System.Collections.Generic;
using System.Text.Json;
using DKX.JsonApi;
using DKX.JsonApi.DI;
using DKX.JsonApiSerializer;
using DKX.JsonApiTests.App.RelationshipLoaders;
using DKX.JsonApiTests.App.V1;

namespace DKX.JsonApiTests.App
{
	public static class ServerFactory
	{
		public static Server Create(IList<string> actions)
		{
			var services = new StaticServiceFactory();
			services.Add(new ChaptersLoaderByChapters(actions));
			services.Add(new RoleLoaderByRole(actions));
			services.Add(new UserLoaderByUser(actions));

			var serializer = new Serializer(new SerializationOptions
			{
				Sorted = true,
				JsonOptions = new JsonWriterOptions
				{
					Indented = true,
				},
			});
			
			var server = new Server(services, serializer);

			server.AddResource<Book, BookMapping>();
			server.AddResource<Chapter, ChapterMapping>();
			server.AddResource<Comment, CommentMapping>();
			server.AddResource<File, FileMapping>();
			server.AddResource<Role, RoleMapping>();
			server.AddResource<User, UserMapping>();

			return server;
		}
	}
}
