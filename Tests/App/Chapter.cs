namespace DKX.JsonApiTests.App
{
	public class Chapter
	{
		public Chapter(string id, string title)
		{
			Id = id;
			Title = title;
		}

		public string Id { get; }
		
		public string Title { get; }

		public User? Author { get; set; }
		
		public User? Editor { get; set; }
	}
}
