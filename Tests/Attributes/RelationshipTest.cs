using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.Relationships.Loaders;
using DKX.JsonApiTests.App;
using Xunit;

namespace DKX.JsonApiTests.Attributes
{
	public class RelationshipTest
	{
		[Fact]
		public void Create_Throws_InvalidLoaderType()
		{
			var e = Assert.Throws<ArgumentException>(() => new Relationship(GetType()));
			Assert.Equal("Relationship loader must implement IRelationshipLoader, but DKX.JsonApiTests.Attributes.RelationshipTest does not", e.Message);
		}

		[Fact]
		public void Getters()
		{
			var loaderType = typeof(TestRelationshipLoader);
			var relationship = new Relationship(loaderType);

			Assert.Equal(loaderType, relationship.LoaderType);
			Assert.Null(relationship.Name);

			relationship = new Relationship(loaderType, "user");

			Assert.Equal("user", relationship.Name);
		}
		
		private class TestRelationshipLoader : IItemRelationshipLoader<Guid, User>
		{
			public Task<IEnumerable<User>> Load(IContext context, IEnumerable<Guid> keys, CancellationToken cancellationToken)
			{
				throw new NotImplementedException();
			}

			public User Extract(IContext context, Guid key, IEnumerable<User> data)
			{
				throw new NotImplementedException();
			}
		}
	}
}
