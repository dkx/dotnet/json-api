using DKX.JsonApi.Attributes;
using Xunit;

namespace DKX.JsonApiTests.Attributes
{
	public class FieldTest : BaseTest
	{
		[Fact]
		public void Getters_WithoutName()
		{
			var field = new Field();
			Assert.Null(field.Name);
		}

		[Fact]
		public void Getters_WithName()
		{
			var field = new Field("name");
			Assert.Equal("name", field.Name);
		}
	}
}
