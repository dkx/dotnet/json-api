using DKX.JsonApi.Attributes;
using Xunit;

namespace DKX.JsonApiTests.Attributes
{
	public class ResourceTest
	{
		[Fact]
		public void Getters()
		{
			var resource = new Resource("v1", "user");

			Assert.Equal("v1", resource.Version);
			Assert.Equal("user", resource.Type);
		}
	}
}
