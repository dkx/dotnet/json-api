using System;
using DKX.JsonApi;
using DKX.JsonApi.Attributes;
using Xunit;

namespace DKX.JsonApiTests.Attributes
{
	public class TransformerTest
	{
		[Fact]
		public void Create_Throws_InvalidTransformer()
		{
			var e = Assert.Throws<ArgumentException>(() => new Transformer(GetType()));
			Assert.Equal("Field transformer must implement IFieldTransformer, but DKX.JsonApiTests.Attributes.TransformerTest does not", e.Message);
		}

		[Fact]
		public void Getters()
		{
			var transformerType = typeof(TestTransformer);
			var transformer = new Transformer(transformerType);

			Assert.Equal(transformerType, transformer.FieldTransformer);
		}
		
		private class TestTransformer : IFieldTransformer<int, int>
		{
			public int Transform(int value)
			{
				throw new NotImplementedException();
			}
		}
	}
}
