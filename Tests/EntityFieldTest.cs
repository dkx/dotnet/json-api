using DKX.JsonApi;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.DI;
using Xunit;

namespace DKX.JsonApiTests
{
	public class EntityFieldTest
	{
		private readonly IValueLoader _valueLoader;

		public EntityFieldTest()
		{
			var serviceFactory = new StaticServiceFactory();
			_valueLoader = new ValueLoader(serviceFactory);
		}

		public string TestingProperty { get; set; } = "042";

		[Fact]
		public void Name_Default()
		{
			var field = new EntityField(_valueLoader, GetType().GetProperty("TestingProperty")!, new Field());
			Assert.Equal("testingProperty", field.Name);
		}

		[Fact]
		public void Name_Custom()
		{
			var field = new EntityField(_valueLoader, GetType().GetProperty("TestingProperty")!, new Field("prop"));
			Assert.Equal("prop", field.Name);
		}

		[Fact]
		public void GetValue()
		{
			var field = new EntityField(_valueLoader, GetType().GetProperty("TestingProperty")!, new Field("prop"));
			Assert.Equal("042", field.GetValue(this));
		}
	}
}
