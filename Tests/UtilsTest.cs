using System;
using DKX.JsonApi;
using Xunit;

namespace DKX.JsonApiTests
{
	public class UtilsTest
	{
		[Fact]
		public void FirstLower()
		{
			Assert.Equal("hELLO", Utils.FirstLower("HELLO"));
		}

		[Fact]
		public void GetMemberType_Throws_NotSupported()
		{
			var e = Assert.Throws<NotSupportedException>(() => Utils.GetMemberType(GetType().GetMethod("GetMemberType_Throws_NotSupported")!));
			Assert.Equal("Can not get type of DKX.JsonApiTests.UtilsTest::GetMemberType_Throws_NotSupported, member type Method is not supported", e.Message);
		}

		public DateTime TestField = DateTime.Now;
		[Fact]
		public void GetMemberType_Field()
		{
			var type = Utils.GetMemberType(GetType().GetField("TestField")!);
			Assert.Equal(typeof(DateTime), type);
		}

		public DateTime? TestFieldNullable = null;
		[Fact]
		public void GetMemberType_Field_Nullable()
		{
			var type = Utils.GetMemberType(GetType().GetField("TestFieldNullable")!);
			Assert.Equal(typeof(Nullable<DateTime>), type);
		}

		public DateTime TestProperty { get; } = DateTime.Now;
		[Fact]
		public void GetMemberType_Property()
		{
			var type = Utils.GetMemberType(GetType().GetProperty("TestProperty")!);
			Assert.Equal(typeof(DateTime), type);
		}

		public DateTime? TestPropertyNullable { get; } = null;
		[Fact]
		public void GetMemberType_Property_Nullable()
		{
			var type = Utils.GetMemberType(GetType().GetProperty("TestPropertyNullable")!);
			Assert.Equal(typeof(Nullable<DateTime>), type);
		}

		public DateTime NotNullableField = DateTime.Now;
		[Fact]
		public void GetNotNullableMemberType()
		{
			var type = Utils.GetNotNullableMemberType(GetType().GetField("NotNullableField")!);
			Assert.Equal(typeof(DateTime), type);
		}

		public DateTime? NullableField = null;
		[Fact]
		public void GetNotNullableMemberType_Null()
		{
			var type = Utils.GetNotNullableMemberType(GetType().GetField("NullableField")!);
			Assert.Equal(typeof(DateTime), type);
		}
	}
}
