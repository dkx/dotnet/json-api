using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.DI;
using DKX.JsonApi.Relationships;
using DKX.JsonApi.Relationships.Loaders;
using DKX.JsonApiSerializer;
using DKX.JsonApiTests.App;
using Xunit;

namespace DKX.JsonApiTests.Relationships
{
	public class EntityRelationshipTest : BaseTest
	{
		private readonly Server _server;

		private readonly StaticServiceFactory _serviceFactory = new StaticServiceFactory();

		private readonly IValueLoader _valueLoader;

		public EntityRelationshipTest()
		{
			_serviceFactory.Add(new LoaderWithCustomItemResult());
			_valueLoader = new ValueLoader(_serviceFactory);
			_server = new Server(_serviceFactory, new Serializer());
		}

		public Guid RelationshipField = Guid.NewGuid();
		
		[Fact]
		public void Create_WithCustomItemResult()
		{
			var relationship = new EntityRelationship<EntityRelationshipTest, Guid>(
				_server,
				_serviceFactory,
				_valueLoader,
				GetType().GetField("RelationshipField")!,
				new Relationship(typeof(LoaderWithCustomItemResult))
			);
		}

		public Guid? NotMatchingTypeField = null;
		
		[Fact]
		public void Create_Throws_NotMatchingKeys()
		{
			var e = Assert.Throws<Exception>(() => new EntityRelationship<EntityRelationshipTest, Guid?>(
				_server,
				_serviceFactory,
				_valueLoader,
				GetType().GetField("NotMatchingTypeField")!,
				new Relationship(typeof(LoaderWithCustomItemResult))
			));

			Assert.Equal("Key type at loader DKX.JsonApiTests.Relationships.EntityRelationshipTest+LoaderWithCustomItemResult (System.Guid) does not match relationship type at mapper DKX.JsonApiTests.Relationships.EntityRelationshipTest::NotMatchingTypeField (System.Nullable`1[System.Guid])", e.Message);
		}

		private class LoaderWithCustomItemResult : IItemRelationshipLoader<Guid, User, User?>
		{
			public Task<IEnumerable<User>> Load(IContext context, IEnumerable<Guid> keys, CancellationToken cancellationToken)
			{
				throw new System.NotImplementedException();
			}

			public User? Extract(IContext context, Guid key, IEnumerable<User> data)
			{
				throw new System.NotImplementedException();
			}
		}
	}
}
