using System;
using DKX.JsonApi;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.DI;
using Xunit;

namespace DKX.JsonApiTests
{
	public class ValueLoaderTest : BaseTest
	{
		private readonly ValueLoader _valueLoader;

		public ValueLoaderTest()
		{
			var services = new StaticServiceFactory();
			services.Add(new MultiplyTransformer());
			services.Add(new NullTransformer());
			
			_valueLoader = new ValueLoader(services);
		}

		[Fact]
		public void Throws_UnsupportedMember()
		{
			var e = Assert.Throws<NotSupportedException>(() => _valueLoader.GetMemberValue(GetType().GetMethod("Throws_UnsupportedMember")!, this));
			Assert.Equal("Can not get value of DKX.JsonApiTests.ValueLoaderTest::Throws_UnsupportedMember, member type Method is not supported", e.Message);
		}

		public int? FieldNull = null;
		[Fact]
		public void GetMemberValue_Field_Null()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetField("FieldNull")!, this);
			Assert.Null(value);
		}

		public int FieldNotNull = 42;
		[Fact]
		public void GetMemberValue_Field()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetField("FieldNotNull")!, this);
			Assert.Equal(42, value);
		}

		[Transformer(typeof(NullTransformer))]
		[Transformer(typeof(MultiplyTransformer))]
		public int FieldNullTransformer = 42;
		[Fact]
		public void GetMemberValue_Field_NullTransformer()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetField("FieldNullTransformer")!, this);
			Assert.Null(value);
		}

		[Transformer(typeof(MultiplyTransformer))]
		[Transformer(typeof(MultiplyTransformer))]
		[Transformer(typeof(MultiplyTransformer))]
		public int FieldWithTransformers = 42;
		[Fact]
		public void GetMemberValue_Field_Transformers()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetField("FieldWithTransformers")!, this);
			Assert.Equal(336, value);
		}

		public int? PropertyNull { get; } = null;
		[Fact]
		public void GetMemberValue_Property_Null()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetProperty("PropertyNull")!, this);
			Assert.Null(value);
		}

		public int PropertyNotNull { get; } = 42;
		[Fact]
		public void GetMemberValue_Property()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetProperty("PropertyNotNull")!, this);
			Assert.Equal(42, value);
		}

		[Transformer(typeof(NullTransformer))]
		[Transformer(typeof(MultiplyTransformer))]
		public int PropertyNullTransformer { get; } = 42;
		[Fact]
		public void GetMemberValue_Property_NullTransformer()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetProperty("PropertyNullTransformer")!, this);
			Assert.Null(value);
		}

		[Transformer(typeof(MultiplyTransformer))]
		[Transformer(typeof(MultiplyTransformer))]
		[Transformer(typeof(MultiplyTransformer))]
		public int PropertyWithTransformers { get; } = 42;
		[Fact]
		public void GetMemberValue_Property_Transformers()
		{
			var value = _valueLoader.GetMemberValue(GetType().GetProperty("PropertyWithTransformers")!, this);
			Assert.Equal(336, value);
		}

		private class MultiplyTransformer : IFieldTransformer<int, int>
		{
			public int Transform(int value)
			{
				return value * 2;
			}
		}

		private class NullTransformer : IFieldTransformer<int, int?>
		{
			public int? Transform(int value)
			{
				return null;
			}
		}
	}
}
