using DKX.JsonApi.DI;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace DKX.JsonApiTests.DI
{
	public class SystemServiceFactoryTest
	{
		[Fact]
		public void RequireService()
		{
			var collection = new ServiceCollection();
			collection.AddSingleton<TestService>();
			
			var factory = new SystemServiceFactory(collection.BuildServiceProvider());
			
			factory.RequireService(typeof(TestService));
		}

		private class TestService
		{
		}
	}
}
