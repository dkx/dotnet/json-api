using DKX.JsonApi;
using DKX.JsonApi.AspNet;
using DKX.JsonApi.DI;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace DKX.JsonApiTests.DI
{
	public class SystemExtensionTest
	{
		[Fact]
		public void AddDkxJsonApi()
		{
			var collection = new ServiceCollection();
			collection.AddDkxJsonApi();

			var services = collection.BuildServiceProvider();
			
			services.GetRequiredService<IServer>();
			services.GetRequiredService<TransformerMiddleware>();
		}
	}
}
