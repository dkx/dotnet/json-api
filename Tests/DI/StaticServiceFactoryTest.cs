using System;
using DKX.JsonApi.DI;
using Xunit;

namespace DKX.JsonApiTests.DI
{
	public class StaticServiceFactoryTest
	{
		private readonly StaticServiceFactory _factory = new StaticServiceFactory();
		
		[Fact]
		public void RequireService_Throws_NotExists()
		{
			var e = Assert.Throws<Exception>(() => _factory.RequireService(GetType()));
			Assert.Equal("Service of type DKX.JsonApiTests.DI.StaticServiceFactoryTest does not exists", e.Message);
		}

		[Fact]
		public void RequireService()
		{
			var service = new TestService();
			_factory.Add(service);
			var found = _factory.RequireService(typeof(TestService));

			Assert.Equal(service, found);
		}

		private class TestService
		{
		}
	}
}
