# Field transformers

```c#
using System;
using DKX.JsonApi.Attributes;

public class Book
{
    public string Id { get; set; }
    public DateTime CreatedAt { get; set; }
}

[Resource("v1", "books")]
public class BookMappingV1
{
    private readonly Book _book;

    public BookMappingV1(Book book)
    {
        _book = book;
    }

    [Id]
    public string Id => _book.Id;

    [Field]
    [Transformer(typeof(DateTimeTransformer))]
    public DateTime CreatedAt => _book.CreatedAt;
}

public class DateTimeTransformer : IFieldTransformer<DateTime, string>
{
    public string Transform(DateTime value)
    {
        return value.ToString("o");
    }
}
```

Each field can have multiple transformers assigned. It works for `[Id]` fields too.
