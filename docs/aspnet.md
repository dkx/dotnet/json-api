# ASP.NET

## Configuration

```c#
using DKX.JsonApi.AspNet;

public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        // ...
        
        app.UseDkxJsonApi();
        
        // ...
    }
}
```

## Return single item

```c#
using DKX.JsonApi.AspNet;

[ApiController]
[Route("/book")]
public class IndexController : ControllerBase
{
    private readonly IBooksRepository _books;
    
    public IndexController(IBooksRepository books)
    {
        _books = books;
    }
    
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        return new ItemResponse<Book>("v1", await _book.GetById("42"));
    }
}
```

## Return collection

```c#
using DKX.JsonApi.AspNet;

[ApiController]
[Route("/books")]
public class IndexController : ControllerBase
{
    private readonly IBooksRepository _books;
    
    public IndexController(IBooksRepository books)
    {
        _books = books;
    }
    
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        return new CollectionResponse<Book>("v1", await _books.GetAll());
    }
}
```

## Return collection with pagination

```c#
using DKX.JsonApi.AspNet;
using DKX.Pagination;

[ApiController]
[Route("/books")]
public class IndexController : ControllerBase
{
    private readonly IBooksRepository _books;

    private readonly IPaginatorFactory _paginatorFactory;
    
    public IndexController(IBooksRepository books, IPaginatorFactory paginatorFactory)
    {
        _books = books;
        _paginatorFactory = paginatorFactory;
    }
    
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        // todo
        var page = 2;
        var itemsPerPage = 20;
        var totalCount = await _books.CountAll();

        var paginator = _paginatorFactory.Create(TotalCount, page, itemsPerPage);
        var books = await _books.GetAll(paginator.PageIndex, paginator.ItemsPerPage);
        var data = new PaginatedData(paginator, books);
        
        return new PaginatedResponse<Book>("v1", data);
    }
}
```

## Without middleware

If you don't want to use the provided middleware, you can try the `JsonApiController` instead.

**example:**

```c#
using DKX.JsonApi;
using DKX.JsonApi.AspNet;

[ApiController]
[Route("/book")]
public class IndexController : JsonApiController
{
    private readonly IBooksRepository _books;
    
    public IndexController(IServer server, IBooksRepository books) : base(server)
    {
        _books = books;
    }
    
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        return await ItemResponse<Book>("v1", await _book.GetById("42"));
    }
}
```
