# Context

`IContext` can be used to access all information about current transformation. It's also used for initial configuration.

```c#
var context = new Context();

// transform item:
var json = server.TransformItem("v1", book, context);

// transform collection:
var json = server.TransformCollection("v1", books, context);
```

**Include relationships:**

```c#
context = context.WithIncludes("author", "chapters.author.role");
```

**Sparse fields:**

```c#
context = context.WithFields("book", "id", "title");
context = context.WithFields("user", "email");
```

This will fetch only `id` and `title` fields for `book` resource and only `email` field for `user` resource.

**Meta data:**

```c#
using DKX.JsonApiSerializer.Value;

var meta = new Struct().Add(
    "pagination",
    new Struct()
        .Add("page", currentPage)
        .Add("itemsPerPage", itemsPerPage)
        .Add("totalCount", totalCount)
);

context = context.WithMeta(meta);
```

## Accessing context within mapper

```c#
[Resource("v1", "books")]
public class BookMappingV1 : IContextAware
{
    private readonly IContext _context;

    private readonly Book _book;

    public BookMappingV1(IContext context, Book book)
    {
        _context = context;
        _book = book;
    }

    [Id]
    public string Id => _book.Id;

    // ...
}
```
