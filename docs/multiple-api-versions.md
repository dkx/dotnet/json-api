# Multiple API versions

```c#
using System;
using DKX.JsonApi.Attributes;

public class Book
{
    public string Id { get; set; }
    public string Title { get; set; }
}

[Resource("v1", "books")]
public class BookMappingV1
{
    private readonly Book _book;

    public BookMappingV1(Book book)
    {
        _book = book;
    }

    [Id]
    public string Id => _book.Id;

    [Field]
    public string Title => _book.Title;
}

[Resource("v2", "books")]
public class BookMappingV2
{
    private readonly Book _book;

    public BookMappingV1(Book book)
    {
        _book = book;
    }

    [Id]
    public string Id => _book.Id;

    // Change Title field to Caption in API V2
    [Field]
    public string Caption => _book.Title;
}

server.AddResource<Book, BookMappingV1>();
server.AddResource<Book, BookMappingV2>();

// later in the app

server.TransformItem("v2", book);
```
