# Relationships

First define relationship loader. It can be either item relationship loader or collection relationship loader.

```c#
class UsersLoader : IItemRelationshipLoader<Guid, User>
{
    private readonly UsersRepository _users;

    public UsersLoader(UsersRepository users)
    {
        _users = users;
    }

    public Task<IEnumerable<User>> Load(IEnumerable<Guid> keys, CancelationToken cancelationToken)
    {
        return _users.FindAllByIdsAsync(keys, cancelationToken);
    }

    public User Extract(Guid key, IEnumerable<User> data)
    {
        return data.First(u => u.Id == key);
    }
}
```

If you want to create collection relationship, implement the `ICollectionRelationshipLoader` interface.

Now you can create relationship on the `User` entity.

```c#
[Resource("v1", "books")]
public class BookMappingV1
{
    private readonly Book _book;

    public BookMappingV1(Book book)
    {
        _book = book;
    }

    [Id]
    public string Id => _book.Id;

    // ...

    [Relationship(typeof(UsersLoader))]
    public Guid Author => _book.AuthorId;
}
```

## N+1 problem with relationships

This library will first collect all relationship ids (keys) and load all data only once (in `Load` method). Later it 
will split loaded data into correct entities (with `Extract` method).

It simply means that N+1 problem is solved automatically by this library.

## Include relationships

**Without ASP.NET:**

```c#
var context = new Context()
    .WithIncludes("author", "chapters.author.role");

var json = server.TransformItem("v1", book, context);
```

**With [ASP.NET](./aspnet.md):**

Simply use the `include` URL query parameter.

```
https://localhost/books?include=author,chapters.author.role
```

Both examples above will automatically include all necessary relationships and generate this structure:

* Book
    * Author
    * Chapters
        * Author
            * Role
