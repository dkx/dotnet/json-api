# Dependency injection

```c#
using DKX.JsonApi.DI;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        // ...
        
        // register custom field transformers
        services.AddScoped<GuidTransformer>();
        services.AddScoped<DateTimeTransformer>();
        
        // configure json:api server
        services.AddDkxJsonApi(server => {

            // map application entities to mappers
            server.AddTransformer<Book, BookMappingV1>();
            server.AddTransformer<Book, BookMappingV2>();
            server.AddTransformer<Book, BookMappingV3>();
        });
        
        // ...
    }
}
```
