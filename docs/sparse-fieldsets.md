# Sparse fieldsets

[JSON:API specification](https://jsonapi.org/format/#fetching-sparse-fieldsets)

Sparse fieldsets is supported with [ASP.NET](./aspnet.md) out of the box. You can use the `fields[]` query parameter.

```
https://localhost/books?include=author&fields[books]=title,body&fields[users]=email
```
