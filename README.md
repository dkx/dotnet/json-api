# DKX/JsonApi

.NET Core [json:api](https://jsonapi.org/) implementation

## Installation

```bash
$ dotnet add package DKX.JsonApi
```

## Basic usage

```c#
using System;
using DKX.JsonApi.Attributes;

public class Book
{
    public string Id { get; set; }
    public string Title { get; set; }
}

[Resource("v1", "books")]
public class BookMappingV1
{
    private readonly Book _book;

    public BookMappingV1(Book book)
    {
        _book = book;
    }

    [Id]
    public string Id => _book.Id;

    [Field]
    public string Title => _book.Title;
}

// there is a service factory version for system DI
var serviceFactory = new StaticServiceFactory();
var server = new Server(serviceFactory);

server.AddResource<Book, BookMappingV1>();

// later in the app

Book book = await books.GetOneById(id);

var json = server.TransformItem<Book>("v1", book);

// or collection of items

IEnumerable<Book> books = await books.GetAll();

var json = server.TransformCollection<Book>("v1", books);
```

## Features

* [Context](./docs/context.md)
* [Dependency injection](./docs/di.md)
* [ASP.NET](./docs/aspnet.md)
* [Relationships](./docs/relationships.md)
* [Sparse fieldsets](./docs/sparse-fieldsets.md)
* [Field transformers](./docs/field-transformers.md)
* [Multiple API versions](./docs/multiple-api-versions.md)
