namespace DKX.JsonApi
{
	internal interface IEntityField
	{
		string Name { get; }

		object? GetValue(object mapper);
	}
}
