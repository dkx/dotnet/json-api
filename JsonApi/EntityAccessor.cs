using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DKX.JsonApi.Relationships;
using DKX.JsonApiSerializer.Relationship;
using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApi
{
	internal class EntityAccessor<TMapper> : IEntityAccessor
		where TMapper : class
	{
		private readonly IEntityConfiguration _configuration;

		private readonly IValueLoader _valueLoader;

		private readonly TMapper _mapper;

		private readonly IDictionary<IEntityRelationship, Func<Task<IRelationship>>> _relationshipFactories = new Dictionary<IEntityRelationship, Func<Task<IRelationship>>>();

		public EntityAccessor(IEntityConfiguration configuration, IValueLoader valueLoader, TMapper mapper)
		{
			_configuration = configuration;
			_valueLoader = valueLoader;
			_mapper = mapper;
		}

		public void PrefetchRelationships(ICycleStorage cycleStorage, string path, string[] includes)
		{
			foreach (var relationship in _configuration.Relationships)
			{
				if (!includes.Contains(relationship.Name))
				{
					continue;
				}
				
				relationship.Prefetch(this, _mapper, cycleStorage, path);
			}
		}

		public void InjectRelationshipFactory(IEntityRelationship relationship, Func<Task<IRelationship>> factory)
		{
			_relationshipFactories.Add(relationship, factory);
		}

		public async Task<JsonApiSerializer.Resource.Item> ToJsonApiItem(IContext context)
		{
			var resourceName = _configuration.ResourceName;
			var item = new JsonApiSerializer.Resource.Item(resourceName, GetId());
			var fields = context.Fields;
			
			foreach (var field in _configuration.Fields)
			{
				if (fields != null && fields.ContainsKey(resourceName) && !fields[resourceName].Contains(field.Name))
				{
					continue;
				}

				item = item.AddAttribute(field.Name, ValueFactory.Create(field.GetValue(_mapper)));
			}

			foreach (var (relationship, factory) in _relationshipFactories)
			{
				var value = await factory();
				item = item.AddRelationship(relationship.Name, value);
			}

			return item;
		}

		private string GetId()
		{
			var id = _valueLoader.GetMemberValue(_configuration.Id, _mapper);
			
			if (id == null)
			{
				throw new Exception($"{_mapper.GetType()}.{_configuration.Id.Name}: returned null, but string was expected");
			}

			if (!(id is string))
			{
				throw new Exception($"{_mapper.GetType()}.{_configuration.Id.Name}: returned type {id.GetType()} but string was expected");
			}

			return (string) id;
		}
	}
}
