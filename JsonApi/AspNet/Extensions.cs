using Microsoft.AspNetCore.Builder;

namespace DKX.JsonApi.AspNet
{
	public static class Extensions
	{
		public static IApplicationBuilder UseDkxJsonApi(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<TransformerMiddleware>();
		}
	}
}
