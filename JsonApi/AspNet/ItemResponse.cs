using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DKX.JsonApi.AspNet
{
	public class ItemResponse<TResult> : IActionResult, IResponse
		where TResult : class
	{
		private readonly string _version;
		
		private readonly TResult _entity;

		public ItemResponse(string version, TResult entity, IContext? context = null)
		{
			_version = version;
			_entity = entity;
			Context = context ?? new Context();
		}

		public Task ExecuteResultAsync(ActionContext context)
		{
			context.HttpContext.Items.Add(TransformerMiddleware.ResponseContextItemsKey, this);
			return Task.CompletedTask;
		}

		public IContext Context { get; }

		public Task<string> TransformToString(IServer server, IContext context)
		{
			return server.TransformItem(_version, _entity, context);
		}
	}
}
