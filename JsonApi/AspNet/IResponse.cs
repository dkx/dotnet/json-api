using System.Threading.Tasks;

namespace DKX.JsonApi.AspNet
{
	public interface IResponse
	{
		IContext Context { get; }
		
		Task<string> TransformToString(IServer server, IContext context);
	}
}
