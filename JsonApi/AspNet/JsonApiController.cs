using System.Collections.Generic;
using System.Threading.Tasks;
using DKX.JsonApiSerializer.Value;
using DKX.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace DKX.JsonApi.AspNet
{
	public class JsonApiController : ControllerBase
	{
		private readonly IServer _server;
		
		protected JsonApiController(IServer server)
		{
			_server = server;
		}

		protected virtual async Task<IActionResult> ItemResponse<TEntity>(string version, TEntity entity, IContext? context = null)
			where TEntity : class
		{
			return JsonApiResult(await _server.TransformItem(version, entity, ContextHelpers.UpdateContextWithHttpRequest(Request, context)));
		}

		protected virtual async Task<IActionResult> CollectionResponse<TEntity>(string version, IEnumerable<TEntity> entities, IContext? context = null)
			where TEntity : class
		{
			return JsonApiResult(await _server.TransformCollection(version, entities, ContextHelpers.UpdateContextWithHttpRequest(Request, context)));
		}

		protected virtual async Task<IActionResult> PaginatedResponse<TEntity>(string version, IPaginatedData<TEntity> data, IContext? context = null)
			where TEntity : class
		{
			var ctx = ContextHelpers.UpdateContextWithHttpRequest(Request, context);
			var pagination = new Struct()
				.Add("page", data.Paginator.Page)
				.Add("itemsPerPage", data.Paginator.ItemsPerPage)
				.Add("totalCount", data.Paginator.TotalCount);

			var meta = ctx.Meta ?? new Struct();
			meta = meta.Add("pagination", pagination);
			ctx = ctx.WithMeta(meta);

			return JsonApiResult(await _server.TransformCollection(version, data.Data, ctx));
		}

		private IActionResult JsonApiResult(string json)
		{
			return Content(json, TransformerMiddleware.ContentType);
		}
	}
}
