using System.Collections.Immutable;
using Microsoft.AspNetCore.Http;

namespace DKX.JsonApi.AspNet
{
	internal class ContextHelpers
	{
		private const string IncludeParameter = "include";

		private const string FieldsParameter = "fields";
		
		public static IContext UpdateContextWithHttpRequest(HttpRequest request, IContext? context)
		{
			var ctx = context ?? new Context();

			return ctx
				.WithIncludes(ExtractIncludes(request))
				.WithFields(ExtractFields(request));
		}

		private static ImmutableArray<string> ExtractIncludes(HttpRequest request)
		{
			if (!request.Query.ContainsKey(IncludeParameter))
			{
				return ImmutableArray<string>.Empty;
			}

			return request.Query[IncludeParameter].ToString().Split(",").ToImmutableArray();
		}

		private static IImmutableDictionary<string, ImmutableArray<string>> ExtractFields(HttpRequest request)
		{
			var query = request.Query;
			var result = ImmutableDictionary.CreateBuilder<string, ImmutableArray<string>>();

			foreach (var (key, value) in query)
			{
				if (!key.StartsWith(FieldsParameter + "[") || !key.EndsWith("]"))
				{
					continue;
				}

				result.Add(
					key.Substring(FieldsParameter.Length + 1, key.Length - FieldsParameter.Length - 2),
					value.ToString().Split(",").ToImmutableArray()
				);
			}

			return result.ToImmutable();
		}
	}
}
