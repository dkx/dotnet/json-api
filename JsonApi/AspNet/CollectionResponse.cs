using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DKX.JsonApi.AspNet
{
	public class CollectionResponse<TResult> : IActionResult, IResponse
		where TResult : class
	{
		private readonly string _version;

		private readonly IEnumerable<TResult> _entities;

		public CollectionResponse(string version, IEnumerable<TResult> entities, IContext? context = null)
		{
			_version = version;
			_entities = entities;
			Context = context ?? new Context();
		}

		public Task ExecuteResultAsync(ActionContext context)
		{
			context.HttpContext.Items.Add(TransformerMiddleware.ResponseContextItemsKey, this);
			return Task.CompletedTask;
		}

		public IContext Context { get; }

		public virtual Task<string> TransformToString(IServer server, IContext context)
		{
			return server.TransformCollection(_version, _entities, context);
		}
	}
}
