using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DKX.JsonApi.AspNet
{
	public class TransformerMiddleware : IMiddleware
	{
		public const string ResponseContextItemsKey = "dkx.jsonapi.response";

		public const string ContentType = "application/vnd.api+json";

		private readonly IServer _server;

		public TransformerMiddleware(IServer server)
		{
			_server = server;
		}

		public async Task InvokeAsync(HttpContext context, RequestDelegate next)
		{
			await next(context);

			if (context.Items.ContainsKey(ResponseContextItemsKey))
			{
				var response = (IResponse) context.Items[ResponseContextItemsKey];
				var ctx = ContextHelpers.UpdateContextWithHttpRequest(context.Request, response.Context);
				var text = await response.TransformToString(_server, ctx);

				context.Response.Headers.Add("Content-Type", ContentType);

				await context.Response.WriteAsync(text);
			}
		}
	}
}
