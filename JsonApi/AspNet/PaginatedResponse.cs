using System.Threading.Tasks;
using DKX.JsonApiSerializer.Value;
using DKX.Pagination;

namespace DKX.JsonApi.AspNet
{
	public class PaginatedResponse<TResult> : CollectionResponse<TResult>
		where TResult : class
	{
		private readonly IPaginatedData<TResult> _data;
		
		public PaginatedResponse(string version, IPaginatedData<TResult> data, IContext? context = null)
			: base(version, data.Data, context)
		{
			_data = data;
		}

		public override Task<string> TransformToString(IServer server, IContext context)
		{
			var pagination = new Struct()
				.Add("page", _data.Paginator.Page)
				.Add("itemsPerPage", _data.Paginator.ItemsPerPage)
				.Add("totalCount", _data.Paginator.TotalCount);

			var meta = context.Meta ?? new Struct();
			meta = meta.Add("pagination", pagination);
			context = context.WithMeta(meta);

			return base.TransformToString(server, context);
		}
	}
}
