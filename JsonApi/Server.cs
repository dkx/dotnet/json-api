using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi.DI;
using DKX.JsonApi.Exceptions;
using DKX.JsonApi.Relationships;
using DKX.JsonApiSerializer;
using DKX.JsonApiSerializer.Resource;
using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApi
{
	public class Server : IServer
	{
		private readonly IServiceFactory _serviceFactory;
		
		private readonly Serializer _serializer;

		private readonly IEntityMapperFactory _entityMapperFactory;

		private readonly IValueLoader _valueLoader;

		private IImmutableDictionary<Type, IImmutableList<IEntityConfiguration>> _mapping = ImmutableDictionary<Type, IImmutableList<IEntityConfiguration>>.Empty;

		public Server(IServiceFactory serviceFactory, Serializer serializer, IEntityMapperFactory? entityMapperFactory = null)
		{
			_serviceFactory = serviceFactory;
			_serializer = serializer;
			_entityMapperFactory = entityMapperFactory ?? new DefaultEntityMapperFactory();
			_valueLoader = new ValueLoader(_serviceFactory);
		}

		public Server(IServiceFactory serviceFactory)
			: this(serviceFactory, new Serializer())
		{
		}
		
		public void AddResource<TEntity, TMapper>()
			where TEntity : class
			where TMapper : class
		{
			var sourceType = typeof(TEntity);
			var mapperType = typeof(TMapper);

			if (!_mapping.ContainsKey(sourceType))
			{
				_mapping = _mapping.Add(sourceType, ImmutableList<IEntityConfiguration>.Empty);
			}

			_mapping = _mapping.SetItem(
				sourceType,
				_mapping[sourceType].Add(new EntityConfiguration<TMapper>(this, _serviceFactory, _entityMapperFactory, _valueLoader, mapperType))
			);
		}

		public async Task<string> TransformItem<TEntity>(string version, TEntity entity, IContext? context = null, CancellationToken cancellationToken = default)
			where TEntity : class
		{
			var ctx = (context ?? new Context()).WithVersion(version);
			var items = await ProcessEntities(ctx, new[]
			{
				GetEntityAccessor(ctx, entity),
			}, cancellationToken);

			if (items.Length != 1)
			{
				throw new ShouldNotHappenException();
			}

			return SerializeData(items[0], ctx.Meta);
		}

		public async Task<string> TransformCollection<TEntity>(string version, IEnumerable<TEntity> collection, IContext? context = null, CancellationToken cancellationToken = default)
			where TEntity : class
		{
			var ctx = (context ?? new Context()).WithVersion(version);
			var items = await ProcessEntities(ctx, collection.Select(e => GetEntityAccessor(ctx, e)).ToArray(), cancellationToken);

			return SerializeData(new Collection(items), ctx.Meta);
		}

		internal IEntityAccessor GetEntityAccessor<TEntity>(IContext context, TEntity entity)
			where TEntity : class
		{
			return GetEntityConfiguration<TEntity>(context.Version).CreateEntityAccessor(context, entity);
		}

		private static async Task<Item[]> ProcessEntities(IContext context, IEnumerable<IEntityAccessor> entities, CancellationToken cancellationToken)
		{
			var array = entities as IEntityAccessor[] ?? entities.ToArray();
			
			await FetchRelationships(context, new Dictionary<string, IEnumerable<IEntityAccessor>>
			{
				[""] = array,
			}, cancellationToken);

			var result = new Item[array.Length];
			for (var i = 0; i < array.Length; i++)
			{
				result[i] = await array[i].ToJsonApiItem(context);
			}

			return result;
		}

		private static async Task FetchRelationships(IContext context, IDictionary<string, IEnumerable<IEntityAccessor>> entitiesGroups, CancellationToken cancellationToken)
		{
			ICycleStorage cycleStorage = new CycleStorage(context);
			
			foreach (var (path, entities) in entitiesGroups)
			{
				foreach (var entity in entities)
				{
					var includes = GetIncludesForPath(context, path);
					entity.PrefetchRelationships(cycleStorage, path, includes);
				}
			}

			var nextCycleFetch = await cycleStorage.FetchAllRelationships(cancellationToken);

			if (nextCycleFetch.Count > 0)
			{
				await FetchRelationships(context, nextCycleFetch, cancellationToken);
			}
		}

		private static string[] GetIncludesForPath(IContext context, string path)
		{
			if (context.Includes == null || context.Includes.Value.Length == 0)
			{
				return new string[] { };
			}

			var pathTest = new Regex(@$"^{Regex.Escape(path)}.([a-zA-Z0-9-_]+)");
			var includes = new List<string>();

			foreach (var include in context.Includes)
			{
				if (path == "")
				{
					includes.Add(include.Split(".")[0]);
					continue;
				}

				var match = pathTest.Match(include);
				if (match.Success)
				{
					includes.Add(match.Groups[1].Value);
				}
			}

			return includes.Distinct().ToArray();
		}

		private IEntityConfiguration GetEntityConfiguration<TEntity>(string version)
			where TEntity : class
		{
			var entityType = typeof(TEntity);
			
			var (_, value) = _mapping.FirstOrDefault(m => m.Key == entityType);
			if (value == null)
			{
				throw new Exception($"Unknown JsonApi entity type {entityType}, type must be registered with Server.AddResource<{entityType}, {entityType}Mapping>()");
			}

			var mappings = value.ToArray();
			if (mappings.Length == 0)
			{
				throw new Exception($"Entity {entityType} does not have any registered mappings");
			}

			var mapping = mappings.FirstOrDefault(m => m.Version == version);
			if (mapping == null)
			{
				throw new Exception($"Entity {entityType} does not have mapping for version {version}");
			}

			return mapping;
		}

		private string SerializeData(IDocumentDataResource data, Struct? meta)
		{
			var document = new Document(data);

			if (meta != null && meta.Data.Count > 0)
			{
				document = document.WithMeta(new Meta(meta));
			}

			return _serializer.Serialize(document);
		}
	}
}
