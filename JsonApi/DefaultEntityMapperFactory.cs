using System;

namespace DKX.JsonApi
{
	public class DefaultEntityMapperFactory : IEntityMapperFactory
	{
		public virtual TMapper CreateInstance<TEntity, TMapper>(IContext context, TEntity entity)
			where TEntity : class
			where TMapper : class
		{
			var type = typeof(TMapper);
			var contextAware = typeof(IContextAware).IsAssignableFrom(typeof(TMapper));
			var args = contextAware ? new object[] {context, entity} : new object[] {entity};
			var mapper = (TMapper?) Activator.CreateInstance(type, args);
			
			if (mapper == null)
			{
				throw new Exception($"Could not create mapper {type} for entity {entity}");
			}

			return mapper;
		}
	}
}
