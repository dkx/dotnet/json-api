namespace DKX.JsonApi
{
	public interface IEntityMapperFactory
	{
		TMapper CreateInstance<TEntity, TMapper>(IContext context, TEntity entity)
			where TEntity : class
			where TMapper : class;
	}
}
