using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApiSerializer.Relationship;

namespace DKX.JsonApi.Relationships.Loaders
{
	internal abstract class RelationshipLoaderWrapper<TKey, TRelationship, TResult> : IRelationshipLoaderWrapper<TKey>
		where TKey : notnull
		where TRelationship : class?
	{
		private readonly IRelationshipLoader<TKey, TRelationship, TResult> _loader;

		protected RelationshipLoaderWrapper(IRelationshipLoader<TKey, TRelationship, TResult> loader)
		{
			_loader = loader;
		}

		protected abstract IEnumerable<IEntityAccessor> DoFetch(IContext context, IEntityAccessor entity, IEntityRelationship relationship, TResult relationshipData);

		public async Task<IEnumerable<IEntityAccessor>> ApplyRelationships(IContext context, IEntityRelationship<TKey> relationship, IDictionary<IEntityAccessor, TKey> keys, CancellationToken cancellationToken)
		{
			var data = (await _loader.Load(context, keys.Select(k => k.Value).Where(k => k != null), cancellationToken)).ToArray();
			var result = new List<IEntityAccessor>();
			
			foreach (var (entity, key) in keys)
			{
				var relationshipData = _loader.Extract(context, key, data);
				if (relationshipData == null)
				{
					FetchNull(entity, relationship);
					continue;
				}

				result.AddRange(DoFetch(context, entity, relationship, relationshipData));
			}

			return result;
		}

		protected void FetchNull(IEntityAccessor entity, IEntityRelationship relationship)
		{
			entity.InjectRelationshipFactory(relationship, () => Task.FromResult(new NullRelationship() as IRelationship));
		}
	}
}
