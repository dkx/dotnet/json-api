using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi.Relationships.Loaders
{
	public interface IRelationshipLoader<in TKey, TRelationship, out TResult>
		where TRelationship : class?
	{
		Task<IEnumerable<TRelationship>> Load(IContext context, IEnumerable<TKey> keys, CancellationToken cancellationToken);
		
		TResult Extract(IContext context, TKey key, IEnumerable<TRelationship> data);
	}
}
