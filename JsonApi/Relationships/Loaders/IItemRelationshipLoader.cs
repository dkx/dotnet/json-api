namespace DKX.JsonApi.Relationships.Loaders
{
	public interface IItemRelationshipLoader<in TKey, TRelationship, out TResult> : IRelationshipLoader<TKey, TRelationship, TResult>
		where TRelationship : class?
		where TResult : class?
	{
	}
	
	public interface IItemRelationshipLoader<in TKey, TRelationship> : IItemRelationshipLoader<TKey, TRelationship, TRelationship>
		where TRelationship : class?
	{
	}
}
