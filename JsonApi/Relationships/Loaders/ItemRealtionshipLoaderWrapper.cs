using System.Collections.Generic;
using DKX.JsonApiSerializer.Relationship;

namespace DKX.JsonApi.Relationships.Loaders
{
	internal class ItemRelationshipLoaderWrapper<TKey, TRelationship, TResult> : RelationshipLoaderWrapper<TKey, TRelationship, TResult>
		where TKey : notnull
		where TRelationship : class?
		where TResult : class?
	{
		private readonly Server _server;
		
		public ItemRelationshipLoaderWrapper(Server server, IItemRelationshipLoader<TKey, TRelationship, TResult> loader)
			: base(loader)
		{
			_server = server;
		}

		protected override IEnumerable<IEntityAccessor> DoFetch(IContext context, IEntityAccessor entity, IEntityRelationship relationship, TResult relationshipData)
		{
			if (relationshipData == null)
			{
				FetchNull(entity, relationship);
				return new IEntityAccessor[] { };
			}
			
			var relationshipEntity = _server.GetEntityAccessor(context, relationshipData!);
			entity.InjectRelationshipFactory(relationship, async () => new ItemRelationship(await relationshipEntity.ToJsonApiItem(context)));

			return new[] {relationshipEntity};
		}
	}
}
