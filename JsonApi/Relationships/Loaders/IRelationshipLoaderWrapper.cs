
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi.Relationships.Loaders
{
	internal interface IRelationshipLoaderWrapper<TKey>
		where TKey : notnull
	{
		Task<IEnumerable<IEntityAccessor>> ApplyRelationships(IContext context, IEntityRelationship<TKey> relationship, IDictionary<IEntityAccessor, TKey> keys, CancellationToken cancellationToken);
	}
}
