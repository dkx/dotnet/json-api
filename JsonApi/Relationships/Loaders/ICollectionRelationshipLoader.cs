using System.Collections.Generic;

namespace DKX.JsonApi.Relationships.Loaders
{
	public interface ICollectionRelationshipLoader<in TKey, TRelationship, out TResult> : IRelationshipLoader<TKey, TRelationship, IEnumerable<TResult>>
		where TRelationship : class?
		where TResult : class?
	{
	}
	
	public interface ICollectionRelationshipLoader<in TKey, TRelationship> : ICollectionRelationshipLoader<TKey, TRelationship, TRelationship>
		where TRelationship : class?
	{
	}
}
