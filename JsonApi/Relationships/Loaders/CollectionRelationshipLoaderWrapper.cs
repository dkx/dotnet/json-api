using System.Collections.Generic;
using DKX.JsonApiSerializer.Relationship;

namespace DKX.JsonApi.Relationships.Loaders
{
	internal class CollectionRelationshipLoaderWrapper<TKey, TRelationship, TResult> : RelationshipLoaderWrapper<TKey, TRelationship, IEnumerable<TResult>>
		where TKey : notnull
		where TRelationship : class?
		where TResult : class?
	{
		private readonly Server _server;
		
		public CollectionRelationshipLoaderWrapper(Server server, ICollectionRelationshipLoader<TKey, TRelationship, TResult> loader)
			: base(loader)
		{
			_server = server;
		}

		protected override IEnumerable<IEntityAccessor> DoFetch(IContext context, IEntityAccessor entity, IEntityRelationship relationship, IEnumerable<TResult> relationshipData)
		{
			var result = new List<IEntityAccessor>();

			foreach (var relationshipItem in relationshipData)
			{
				if (relationshipItem == null)
				{
					continue;
				}
				
				var innerRelationshipEntity = _server.GetEntityAccessor(context, relationshipItem!);
				result.Add(innerRelationshipEntity);
			}
					
			entity.InjectRelationshipFactory(relationship, async () =>
			{
				var items = new List<ItemRelationship>();
				foreach (var item in result)
				{
					items.Add(new ItemRelationship(await item.ToJsonApiItem(context)));
				}
						
				return new CollectionRelationship(items);
			});

			return result;
		}
	}
}
