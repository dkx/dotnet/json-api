using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi.Relationships
{
	internal interface IEntityRelationship
	{
		string Name { get; }

		void Prefetch(IEntityAccessor entity, object mapper, ICycleStorage cycleStorage, string parentPath);
	}
	
	internal interface IEntityRelationship<TKey> : IEntityRelationship
		where TKey : notnull
	{
		Task<IEnumerable<IEntityAccessor>> ApplyRelationships(IContext context, IDictionary<IEntityAccessor, TKey> keys, CancellationToken cancellationToken);
	}
}
