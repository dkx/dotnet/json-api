using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi.Relationships
{
	internal class CycleStorage : ICycleStorage
	{
		private readonly IContext _context;

		private readonly IDictionary<Type, IDictionary<IEntityRelationship, IKeysStorage>> _keys = new Dictionary<Type, IDictionary<IEntityRelationship, IKeysStorage>>();

		public CycleStorage(IContext context)
		{
			_context = context;
		}

		public void StoreKey<TMapper, TKey>(IEntityAccessor entity, IEntityRelationship<TKey> relationship, TKey key, string parentPath)
			where TMapper : class
			where TKey : notnull
		{
			var mapperType = typeof(TMapper);
			
			if (!_keys.ContainsKey(mapperType))
			{
				_keys.Add(mapperType, new Dictionary<IEntityRelationship, IKeysStorage>());
			}

			if (!_keys[mapperType].ContainsKey(relationship))
			{
				_keys[mapperType].Add(relationship, new KeysStorage<TKey>(_context, relationship, parentPath));
			}

			(_keys[mapperType][relationship] as KeysStorage<TKey>)!.Add(entity, key);
		}

		public async Task<IDictionary<string, IEnumerable<IEntityAccessor>>> FetchAllRelationships(CancellationToken cancellationToken)
		{
			var result = new Dictionary<string, IEnumerable<IEntityAccessor>>();

			foreach (var (_, relationships) in _keys)
			{
				foreach (var (_, keys) in relationships)
				{
					result.Add(keys.FullPath, await keys.FetchRelationships(cancellationToken));
				}
			}

			return result;
		}
	}
}
