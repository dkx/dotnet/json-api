using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi.Relationships
{
	internal class KeysStorage<TKey> : IKeysStorage
		where TKey : notnull
	{
		private readonly IContext _context;
		
		private readonly IEntityRelationship<TKey> _relationship;

		private readonly IDictionary<IEntityAccessor, TKey> _keys = new Dictionary<IEntityAccessor, TKey>();

		public KeysStorage(IContext context, IEntityRelationship<TKey> relationship, string parentPath)
		{
			_context = context;
			_relationship = relationship;
			FullPath = parentPath == "" ? relationship.Name : parentPath + "." + relationship.Name;
		}

		public void Add(IEntityAccessor entity, TKey key)
		{
			_keys.Add(entity, key);
		}
		
		public string FullPath { get; }

		public Task<IEnumerable<IEntityAccessor>> FetchRelationships(CancellationToken cancellationToken)
		{
			return _relationship.ApplyRelationships(_context, _keys, cancellationToken);
		}
	}
}
