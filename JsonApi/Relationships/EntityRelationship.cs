using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.DI;
using DKX.JsonApi.Exceptions;
using DKX.JsonApi.Relationships.Loaders;
using DKX.JsonApiSerializer.Relationship;

namespace DKX.JsonApi.Relationships
{
	internal class EntityRelationship<TMapper, TKey> : IEntityRelationship<TKey>
		where TKey : notnull
		where TMapper : class
	{
		private readonly Server _server;
		
		private readonly IServiceFactory _serviceFactory;
		
		private readonly IValueLoader _valueLoader;
		
		private readonly Relationship _relationship;

		private readonly MemberInfo _member;

		private readonly IRelationshipLoaderWrapper<TKey> _loader;

		public EntityRelationship(Server server, IServiceFactory serviceFactory, IValueLoader valueLoader, MemberInfo member, Relationship relationship)
		{
			_server = server;
			_serviceFactory = serviceFactory;
			_valueLoader = valueLoader;
			_member = member;
			_relationship = relationship;
			_loader = CreateRelationshipLoaderWrapper();
			Name = relationship.Name ?? Utils.FirstLower(member.Name);
		}

		public string Name { get; }

		public void Prefetch(IEntityAccessor entity, object mapper, ICycleStorage cycleStorage, string parentPath)
		{
			var key = _valueLoader.GetMemberValue(_member, mapper);
			
			if (key == null)
			{
				entity.InjectRelationshipFactory(this, () => Task.FromResult(new NullRelationship() as IRelationship));
			}
			else
			{
				cycleStorage.StoreKey<TMapper, TKey>(entity, this, (TKey) key, parentPath);
			}
		}

		public Task<IEnumerable<IEntityAccessor>> ApplyRelationships(IContext context, IDictionary<IEntityAccessor, TKey> keys, CancellationToken cancellationToken)
		{
			return _loader.ApplyRelationships(context, this, keys, cancellationToken);
		}

		private IRelationshipLoaderWrapper<TKey> CreateRelationshipLoaderWrapper()
		{
			var loader = _serviceFactory.RequireService(_relationship.LoaderType);
			var interfaces = loader.GetType().GetInterfaces();

			var itemLoaderType = typeof(IItemRelationshipLoader<,,>);
			var itemInterface = interfaces.FirstOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == itemLoaderType);
			if (itemInterface != null)
			{
				CheckKeyType(itemInterface.GetGenericArguments()[0]);
				
				var wrapper = Activator.CreateInstance(
					typeof(ItemRelationshipLoaderWrapper<,,>).MakeGenericType(itemInterface.GetGenericArguments()),
					_server,
					loader
				);

				if (wrapper == null)
				{
					throw new ShouldNotHappenException();
				}

				return (IRelationshipLoaderWrapper<TKey>) wrapper;
			}

			var collectionLoaderType = typeof(ICollectionRelationshipLoader<,,>);
			var collectionInterface = interfaces.FirstOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == collectionLoaderType);
			if (collectionInterface != null)
			{
				CheckKeyType(collectionInterface.GetGenericArguments()[0]);
				
				var wrapper = Activator.CreateInstance(
					typeof(CollectionRelationshipLoaderWrapper<,,>).MakeGenericType(collectionInterface.GetGenericArguments()),
					_server,
					loader
				);

				if (wrapper == null)
				{
					throw new ShouldNotHappenException();
				}

				return (IRelationshipLoaderWrapper<TKey>) wrapper;
			}

			throw new NotSupportedException($"Unsupported relationship loader {loader.GetType().FullName}");
		}

		private void CheckKeyType(Type loaderKeyType)
		{
			if (loaderKeyType != typeof(TKey))
			{
				throw new Exception($"Key type at loader {_relationship.LoaderType.FullName} ({loaderKeyType}) does not match relationship type at mapper {typeof(TMapper).FullName}::{_member.Name} ({typeof(TKey)})");
			}
		}
	}
}
