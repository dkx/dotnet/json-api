using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi.Relationships
{
	internal interface ICycleStorage
	{
		void StoreKey<TMapper, TKey>(IEntityAccessor entity, IEntityRelationship<TKey> relationship, TKey key, string parentPath)
			where TMapper : class
			where TKey : notnull;

		Task<IDictionary<string, IEnumerable<IEntityAccessor>>> FetchAllRelationships(CancellationToken cancellationToken);
	}
}
