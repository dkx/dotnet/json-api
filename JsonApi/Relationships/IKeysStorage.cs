using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi.Relationships
{
	internal interface IKeysStorage
	{
		string FullPath { get; }
		
		Task<IEnumerable<IEntityAccessor>> FetchRelationships(CancellationToken cancellationToken);
	}
}
