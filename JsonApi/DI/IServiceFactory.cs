using System;

namespace DKX.JsonApi.DI
{
	public interface IServiceFactory
	{
		object RequireService(Type type);
	}
}
