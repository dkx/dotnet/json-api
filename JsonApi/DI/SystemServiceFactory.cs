using System;
using Microsoft.Extensions.DependencyInjection;

namespace DKX.JsonApi.DI
{
	public class SystemServiceFactory : IServiceFactory
	{
		private readonly IServiceProvider _services;

		public SystemServiceFactory(IServiceProvider services)
		{
			_services = services;
		}
		
		public object RequireService(Type type)
		{
			return _services.GetRequiredService(type);
		}
	}
}
