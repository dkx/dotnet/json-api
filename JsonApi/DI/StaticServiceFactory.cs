using System;
using System.Collections.Generic;
using System.Linq;

namespace DKX.JsonApi.DI
{
	public class StaticServiceFactory : IServiceFactory
	{
		private readonly IDictionary<Type, object> _services = new Dictionary<Type, object>();
		
		public void Add<TService>(TService service)
			where TService : class
		{
			_services.Add(typeof(TService), service);
		}
		
		public object RequireService(Type type)
		{
			var (_, service) = _services.FirstOrDefault(s => s.Key == type);
			if (service == null)
			{
				throw new Exception($"Service of type {type} does not exists");
			}

			return service;
		}
	}
}
