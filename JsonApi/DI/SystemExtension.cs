using System;
using DKX.JsonApi.AspNet;
using DKX.JsonApiSerializer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace DKX.JsonApi.DI
{
	public static class SystemExtensions
	{
		public static IServiceCollection AddDkxJsonApi(this IServiceCollection services, ExtensionsOptions options, Action<IServer> configure)
		{
			services.TryAddSingleton<IEntityMapperFactory>(provider => new DefaultEntityMapperFactory());
			
			services.TryAddScoped<IServer>(provider =>
			{
				var serviceFactory = new SystemServiceFactory(provider);
				var serializer = new Serializer(options);
				var entityMapperFactory = provider.GetRequiredService<IEntityMapperFactory>();
				var server = new Server(serviceFactory, serializer, entityMapperFactory);

				configure.Invoke(server);

				return server;
			});

			services.AddTransient<TransformerMiddleware>();

			return services;
		}

		public static IServiceCollection AddDkxJsonApi(this IServiceCollection services, Action<IServer> configure)
		{
			return AddDkxJsonApi(services, new ExtensionsOptions(), configure);
		}

		public static IServiceCollection AddDkxJsonApi(this IServiceCollection services)
		{
			return AddDkxJsonApi(services, new ExtensionsOptions(), server => {});
		}
	}

	public class ExtensionsOptions : SerializationOptions {}
}
