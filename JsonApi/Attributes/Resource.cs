using System;

namespace DKX.JsonApi.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class Resource : Attribute
	{
		public Resource(string version, string type)
		{
			Version = version;
			Type = type;
		}
		
		public string Version { get; }

		public string Type { get; }
	}
}
