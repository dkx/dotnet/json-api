using System;
using System.Linq;
using DKX.JsonApi.Relationships.Loaders;

namespace DKX.JsonApi.Attributes
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class Relationship : Attribute
	{
		private static readonly Type RelationshipLoaderType = typeof(IRelationshipLoader<,,>);

		public Relationship(Type loaderType, string? name = null)
		{
			if (!loaderType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == RelationshipLoaderType))
			{
				throw new ArgumentException($"Relationship loader must implement IRelationshipLoader, but {loaderType} does not");
			}

			LoaderType = loaderType;
			Name = name;
		}
		
		public Type LoaderType { get; }

		public string? Name { get; }
	}
}
