using System;
using System.Linq;

namespace DKX.JsonApi.Attributes
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true)]
	public class Transformer : Attribute
	{
		private static readonly Type FieldTransformerType = typeof(IFieldTransformer<,>);
		
		public Transformer(Type fieldTransformer)
		{
			if (!fieldTransformer.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == FieldTransformerType))
			{
				throw new ArgumentException($"Field transformer must implement IFieldTransformer, but {fieldTransformer} does not");
			}
			
			FieldTransformer = fieldTransformer;
		}
		
		public Type FieldTransformer { get; }
	}
}
