using System;

namespace DKX.JsonApi.Attributes
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class Id : Attribute
	{
		public Id() {}
	}
}
