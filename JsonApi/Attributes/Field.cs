using System;

namespace DKX.JsonApi.Attributes
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class Field : Attribute
	{
		public Field(string? name = null)
		{
			Name = name;
		}

		public string? Name { get; }
	}
}
