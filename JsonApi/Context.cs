using System.Collections.Generic;
using System.Collections.Immutable;
using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApi
{
	public class Context : IContext
	{
		public Context(
			string version = "v1",
			ImmutableArray<string>? includes = null,
			IImmutableDictionary<string, ImmutableArray<string>>? fields = null,
			Struct? meta = null,
			IDictionary<string, object?>? items = null
		)
		{
			Version = version;
			Includes = includes;
			Fields = fields;
			Meta = meta;
			Items = items ?? new Dictionary<string, object?>();
		}

		public string Version { get; }
		
		public ImmutableArray<string>? Includes { get; }
		
		public IImmutableDictionary<string, ImmutableArray<string>>? Fields { get; }
		
		public Struct? Meta { get; }

		public IDictionary<string, object?> Items { get; }

		public IContext WithVersion(string version)
		{
			return new Context(version, Includes, Fields, Meta, Items);
		}

		public IContext WithIncludes(params string[] includes)
		{
			return WithIncludes(ImmutableArray.Create(includes));
		}

		public IContext WithIncludes(ImmutableArray<string> includes)
		{
			return new Context(Version, includes, Fields, Meta, Items);
		}

		public IContext WithFields(string resourceName, params string[] fields)
		{
			return WithFields(resourceName, ImmutableArray.Create(fields));
		}

		public IContext WithFields(string resourceName, ImmutableArray<string> fields)
		{
			var all = Fields ?? ImmutableDictionary<string, ImmutableArray<string>>.Empty;
			if (all.ContainsKey(resourceName))
			{
				all = all.Remove(resourceName);
			}

			all = all.Add(resourceName, fields);
			
			return WithFields(all);
		}

		public IContext WithFields(IImmutableDictionary<string, ImmutableArray<string>> fields)
		{
			return new Context(Version, Includes, fields, Meta, Items);
		}

		public IContext WithMeta(Struct meta)
		{
			return new Context(Version, Includes, Fields, meta, Items);
		}

		public IContext WithItems(IDictionary<string, object?> items)
		{
			foreach (var (key, value) in items)
			{
				Items.Add(key, value);
			}

			return this;
		}
	}
}
