using System;

namespace DKX.JsonApi.Exceptions
{
	public class ShouldNotHappenException : Exception
	{
		public ShouldNotHappenException() : base("Should not happen") {}
	}
}
