using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.JsonApi
{
	public interface IServer
	{
		void AddResource<TEntity, TMapper>()
			where TEntity : class
			where TMapper : class;

		Task<string> TransformItem<TEntity>(string version, TEntity entity, IContext? context = null, CancellationToken cancellationToken = default)
			where TEntity : class;

		Task<string> TransformCollection<TEntity>(string version, IEnumerable<TEntity> collection, IContext? context = null, CancellationToken cancellationToken = default)
			where TEntity : class;
	}
}
