using System;
using System.Linq;
using System.Reflection;

namespace DKX.JsonApi
{
	internal static class Utils
	{
		public static string FirstLower(string str)
		{
			return str.First().ToString().ToLower() + str.Substring(1);
		}

		public static Type GetMemberType(MemberInfo member)
		{
			return member.MemberType switch
			{
				MemberTypes.Field => ((FieldInfo) member).FieldType,
				MemberTypes.Property => ((PropertyInfo) member).PropertyType,
				_ => throw new NotSupportedException($"Can not get type of {member.DeclaringType!.FullName}::{member.Name}, member type {member.MemberType} is not supported"),
			};
		}

		public static Type GetNotNullableMemberType(MemberInfo member)
		{
			var type = GetMemberType(member);
			var realType = Nullable.GetUnderlyingType(type);

			return realType ?? type;
		}
	}
}
