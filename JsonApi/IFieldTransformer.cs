namespace DKX.JsonApi
{
	public interface IFieldTransformer<in TValue, out TResult>
	{
		public TResult Transform(TValue value);
	}
}
