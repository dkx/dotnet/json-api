using System;
using System.Reflection;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.DI;
using DKX.JsonApi.Exceptions;

namespace DKX.JsonApi
{
	internal class ValueLoader : IValueLoader
	{
		private readonly IServiceFactory _serviceFactory;

		public ValueLoader(IServiceFactory serviceFactory)
		{
			_serviceFactory = serviceFactory;
		}
		
		public object? GetMemberValue(MemberInfo member, object obj)
		{
			var value = member.MemberType switch
			{
				MemberTypes.Field => ((FieldInfo) member).GetValue(obj),
				MemberTypes.Property => ((PropertyInfo) member).GetValue(obj),
				_ => throw new NotSupportedException($"Can not get value of {member.DeclaringType!.FullName}::{member.Name}, member type {member.MemberType} is not supported"),
			};

			if (value == null)
			{
				return null;
			}

			var transformerAttributeType = typeof(Transformer);
			var transformerAttributes = (Transformer[]) Attribute.GetCustomAttributes(member, transformerAttributeType);

			foreach (var transformerAttribute in transformerAttributes)
			{
				var transformerType = transformerAttribute.FieldTransformer;
				var transformer = _serviceFactory.RequireService(transformerType);
				var transform = transformer.GetType().GetMethod("Transform");
				if (transform == null)
				{
					throw new ShouldNotHappenException();
				}

				value = transform.Invoke(transformer, new[] {value});

				if (value == null)
				{
					return null;
				}
			}

			return value;
		}
	}
}
