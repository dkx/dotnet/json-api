using System;
using System.Threading.Tasks;
using DKX.JsonApi.Relationships;
using DKX.JsonApiSerializer.Relationship;

namespace DKX.JsonApi
{
	internal interface IEntityAccessor
	{
		void PrefetchRelationships(ICycleStorage cycleStorage, string path, string[] includes);

		void InjectRelationshipFactory(IEntityRelationship relationship, Func<Task<IRelationship>> factory);

		Task<JsonApiSerializer.Resource.Item> ToJsonApiItem(IContext context);
	}
}
