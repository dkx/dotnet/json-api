using System;
using System.Collections.Immutable;
using System.Reflection;
using DKX.JsonApi.Attributes;
using DKX.JsonApi.DI;
using DKX.JsonApi.Exceptions;
using DKX.JsonApi.Relationships;

namespace DKX.JsonApi
{
	internal class EntityConfiguration<TMapper> : IEntityConfiguration
		where TMapper : class
	{
		private readonly Server _server;
		
		private readonly IServiceFactory _serviceFactory;

		private readonly IEntityMapperFactory _entityMapperFactory;
		
		private readonly IValueLoader _valueLoader;
		
		private readonly Type _mappingType;

		private readonly Resource _resource;

		public EntityConfiguration(Server server, IServiceFactory serviceFactory, IEntityMapperFactory entityMapperFactory, IValueLoader valueLoader, Type mappingType)
		{
			var resource = (Resource?) Attribute.GetCustomAttribute(mappingType, typeof(Resource));

			_server = server;
			_serviceFactory = serviceFactory;
			_entityMapperFactory = entityMapperFactory;
			_valueLoader = valueLoader;
			_mappingType = mappingType;
			_resource = resource ?? throw new ArgumentException($"Class {mappingType.FullName} is not valid entity mapper because it is missing a [Resource] attribute");
			Id = ExtractId();
			Fields = ExtractFields();
			Relationships = ExtractRelationships();
		}

		public string Version => _resource.Version;

		public string ResourceName => _resource.Type;

		public MemberInfo Id { get; }

		public ImmutableArray<IEntityField> Fields { get; }

		public ImmutableArray<IEntityRelationship> Relationships { get; }
		
		public IEntityAccessor CreateEntityAccessor<TEntity>(IContext context, TEntity entity)
			where TEntity : class
		{
			return new EntityAccessor<TMapper>(this, _valueLoader, _entityMapperFactory.CreateInstance<TEntity, TMapper>(context, entity));
		}

		private MemberInfo ExtractId()
		{
			var members = _mappingType.GetMembers(BindingFlags.Instance | BindingFlags.Public);
			var idType = typeof(Id);

			foreach (var member in members)
			{
				var id = (Id?) Attribute.GetCustomAttribute(member, idType);
				if (id != null)
				{
					return member;
				}
			}

			throw new Exception($"Entity mapper {_mappingType.FullName} does not have any member with [Id] attribute");
		}

		private ImmutableArray<IEntityField> ExtractFields()
		{
			var members = _mappingType.GetMembers(BindingFlags.Instance | BindingFlags.Public);
			var fieldType = typeof(Field);
			var result = ImmutableArray.CreateBuilder<IEntityField>();

			foreach (var member in members)
			{
				var field = (Field?) Attribute.GetCustomAttribute(member, fieldType);
				if (field != null)
				{
					result.Add(new EntityField(_valueLoader, member, field));
				}
			}

			return result.ToImmutable();
		}

		private ImmutableArray<IEntityRelationship> ExtractRelationships()
		{
			var relationshipType = typeof(Relationship);
			var entityRelationshipType = typeof(EntityRelationship<,>);

			var members = _mappingType.GetMembers(BindingFlags.Instance | BindingFlags.Public);
			var result = ImmutableArray.CreateBuilder<IEntityRelationship>();
			
			foreach (var member in members)
			{
				var relationship = (Relationship?) Attribute.GetCustomAttribute(member, relationshipType);
				if (relationship == null)
				{
					continue;
				}
				
				var memberType = Utils.GetNotNullableMemberType(member);
				var entityRelationship = (IEntityRelationship?) Activator.CreateInstance(
					entityRelationshipType.MakeGenericType(typeof(TMapper), memberType),
					_server,
					_serviceFactory,
					_valueLoader,
					member,
					relationship
				)!;

				if (entityRelationship == null)
				{
					throw new ShouldNotHappenException();
				}

				result.Add(entityRelationship);
			}

			return result.ToImmutable();
		}
	}
}
