using System.Collections.Generic;
using System.Collections.Immutable;
using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApi
{
	public interface IContext
	{
		string Version { get; }
		
		ImmutableArray<string>? Includes { get; }

		IImmutableDictionary<string, ImmutableArray<string>>? Fields { get; }

		Struct? Meta { get; }

		IDictionary<string, object?> Items { get; }

		IContext WithVersion(string version);

		IContext WithIncludes(params string[] includes);
		
		IContext WithIncludes(ImmutableArray<string> includes);

		IContext WithFields(string resourceName, params string[] fields);

		IContext WithFields(string resourceName, ImmutableArray<string> fields);

		IContext WithFields(IImmutableDictionary<string, ImmutableArray<string>> fields);

		IContext WithMeta(Struct meta);

		IContext WithItems(IDictionary<string, object?> items);
	}
}
