using System.Reflection;

namespace DKX.JsonApi
{
	public interface IValueLoader
	{
		object? GetMemberValue(MemberInfo member, object obj);
	}
}
