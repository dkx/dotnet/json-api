using System.Reflection;
using DKX.JsonApi.Attributes;

namespace DKX.JsonApi
{
	internal class EntityField : IEntityField
	{
		private readonly IValueLoader _valueLoader;
		
		private readonly MemberInfo _member;

		public EntityField(IValueLoader valueLoader, MemberInfo member, Field field)
		{
			_valueLoader = valueLoader;
			_member = member;
			Name = field.Name ?? Utils.FirstLower(member.Name);
		}

		public string Name { get; }
		
		public object? GetValue(object mapper)
		{
			return _valueLoader.GetMemberValue(_member, mapper);
		}
	}
}
