using System.Collections.Immutable;
using System.Reflection;
using DKX.JsonApi.Relationships;

namespace DKX.JsonApi
{
	internal interface IEntityConfiguration
	{
		string ResourceName { get; }
		
		string Version { get; }

		MemberInfo Id { get; }

		ImmutableArray<IEntityField> Fields { get; }

		ImmutableArray<IEntityRelationship> Relationships { get; }

		IEntityAccessor CreateEntityAccessor<TEntity>(IContext context, TEntity entity)
			where TEntity : class;
	}
}
